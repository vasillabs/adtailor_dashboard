// ::: Adtailor TODO :::

/* 
	0) Postman handle post request 
	- response XML
	1) SASS structure / Bootstrap / Materialize  
	2) Header with nav / sidebar 
	3) Filter bar 
	4) Table with filter 
	5) Chloropleth Map -- google  
	6) Bars / charts/ tables with user system data 
 */


/* --------------------------------------------
	TODO ::
		1) Navigation 
		2) Filter bar
		3) Add search bar for definitions 
		4) Add common update method to all the tables / FIll with data / change charts 
		5) Transfer old features
		6) Pop up window
		7) Calendar widget
		8) Create reusable component
		9) Add loading animation
*/


/* 
	::::: TODO :::::

	1. Loading animation
	2. Top bar
*/


/* 
	::: TODO
	1) Top bar
	2) reusable components with charts
	3) refactor js with design patterns
	4) filter bar widgets dropdowns
	5) build XML document with needed data
*/


/*  ::::: Daily :::::
 -----------------------------
 - Top bar + navigtion
 - FIlte widget 
 - XML data
 - show all results chart for first tree columns - visitsl, age , ethnicity
 - show report for single property 
 - clean code - JS 
 - create reusable component -widget with tabs 
 - form 
 - bootstrap widgets
 - login page */




(function () {
	Array.prototype.sum = function (prop) {
		var total = 0
		for (var i = 0, _len = this.length; i < _len; i++) {
			total += this[i][prop]
		}
		return Number(total)
	}
})();


var EasyPie = {
	init: function (data) {
		var self = this;

		data.forEach(function (dataSet) {
			var data = {
				class: dataSet.class,
				options: {
					barColor: dataSet.color,
					scaleColor: false,
					lineWidth: 6,
					size: 60
				}
			};

			var currentPie = $(data.class).easyPieChart(data.options);
		});
	},
	update: function (className, percentage) {
		$(className).data('easyPieChart').update(percentage);
	}
}

var Doughnut = {
	init: function (ctx, data) {
		if (ctx === undefined || data === undefined) return false;

		return new Chart(ctx, {
			type: 'doughnut',
			data: data
		});
	},
	update: function (chart, data) {
		if (chart === undefined || data === undefined) return false;

		chart.data.datasets.forEach((dataset) => {
			dataset.data = data;
		});
		chart.update();
	}
}

var BarChart = {
	init: function (ctx, lineData, labelsArray) {
		if (ctx === undefined || lineData === undefined || labelsArray === undefined) return false;

		var gradientStroke = ctx.createLinearGradient(500, 0, 100, 0);
		var gradientFill = ctx.createLinearGradient(500, 0, 100, 0);
		gradientStroke.addColorStop(0, "#f39c12");
		gradientStroke.addColorStop(1, "#e91e63");
		gradientFill.addColorStop(0, "#f39c12");
		gradientFill.addColorStop(1, "#e91e63");

		var lineOptions = {
			responsive: true,
			maintainAspectRatio: false,
			showScale: false,
			scales: {
				xAxes: [{
					gridLines: {
						display: false,
						drawBorder: false,
					},
					barPercentage: 0.8,
					categoryPercentage: 0.8
				}],
				yAxes: [{
					display: false,
					gridLines: false,
					gridLines: {
						display: false,
						drawBorder: false,
					},
				}]
			},
			legend: {
				display: false
			}
		};

		var lineData = {
			labels: labelsArray,
			datasets: [{
				label: "Data 1",
				backgroundColor: gradientFill,
				hoverBackgroundColor: gradientFill,
				data: lineData
			}]
		};

		return new Chart(ctx, {
			type: 'bar',
			data: lineData,
			options: lineOptions
		});
	}
}

var LeafletMap = (function () {
	var map = {};
	var infoMethods = {};
	var token = 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpandmbXliNDBjZWd2M2x6bDk3c2ZtOTkifQ._QA7i5Mpkd_m30IGElHziw';
	var geojson;
	var map = L.map('map', {
		attributionControl: false
	}).setView([27.8, 0], 2);

	L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png?access_token=' + token + '', {
		maxZoom: 18
	}).addTo(map);

	map.doubleClickZoom.disable();
	map.scrollWheelZoom.disable();

	// --- INFO
	var info = L.control();
	info.onAdd = function (map) {
		this._div = L.DomUtil.create('div', 'info');
		this.update();
		return this._div;
	};

	info.update = function (props) {
		var visitsTopContainer,
			formattedVisitsTopContainer;

		if (props) {
			visitsTopContainer = props.visits;
			if (visitsTopContainer != 0) {
				formattedVisitsTopContainer = visitsTopContainer.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1, ");
				this._div.innerHTML = '<h4>Visits from:</h4><p><b style="color:#1f626e;font-size:15px;">' + props.name + '</b></p><p style="font-size:12px;"><b style="color:#000;font-size:16px">' + props.visitRate + '</b> traffic scale </p><p style="font-size:12px;"><b style="color:#000;font-size:16px">' + formattedVisitsTopContainer + '</b> visits</p>';
			} else {
				this._div.innerHTML = '<h4>Visits from:</h4><p><b style="color:#1f626e;font-size:15px;">' + props.name + '</b></p><p style="font-size:12px;"><b style="color:#000;font-size:16px">' + props.visitRate + '</b> traffic scale </p><p style="font-size:12px;"><b style="color:#000;font-size:16px">0</b> visits</p>';
			}
		} else {
			this._div.innerHTML = 'Hover over a state';
		}
	};

	info.addTo(map);


	// --- LEGEND
	var legend = L.control({
		position: 'bottomright'
	});

	legend.onAdd = function (map) {

		var div = L.DomUtil.create('div', 'info legend'),
			grades = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
			labels = [],
			from;

		for (var i = 0; i < grades.length; i++) {
			from = grades[i];

			labels.push(
				'<i style="background:' + getColor(from + 1) + '"></i> ' +
				from);
		}

		div.innerHTML = labels.join('<br>');
		return div;
	};

	legend.addTo(map);

	function getColor(d) {
		return d == 0 ? '#F9FBE7' :
			d == 1 ? '#BBDEFB' :
			d == 2 ? '#90CAF9' :
			d == 3 ? '#64B5F6' :
			d == 4 ? '#42A5F5' :
			d == 5 ? '#2196F3' :
			d == 6 ? '#1E88E5' :
			d == 7 ? '#1976D2' :
			d == 8 ? '#1565C0' :
			d == 9 ? '#0D47A1' :
			'';
	}

	function style(feature) {
		return {
			weight: 1,
			opacity: .8,
			color: 'white',
			dashArray: '3',
			fillOpacity: 0.7,
			fillColor: getColor(feature.properties.visitRate)
		};
	}

	function highlightFeature(e) {
		var layer = e.target;

		layer.setStyle({
			weight: 2,
			color: '#888',
			dashArray: '',
			fillOpacity: 0.4
		});

		if (!L.Browser.ie && !L.Browser.opera) {
			layer.bringToFront();
		}

		info.update(layer.feature.properties);
	}

	function resetHighlight(e) {
		geojson.resetStyle(e.target);
		info.update();
	}

	function zoomToFeature(e) {
		map.fitBounds(e.target.getBounds());
	}

	function onEachFeature(feature, layer) {
		layer.on({
			mouseover: highlightFeature,
			mouseout: resetHighlight
		});
	}

	geojson = L.geoJson(statesData, {
		style: style,
		onEachFeature: onEachFeature
	}).addTo(map);

	map.update = function (data, visitsAll, used) {
		if (data == undefined || visitsAll == undefined || used == undefined) return false;

		geojson = L.geoJson(statesData, {
			style: style,
			onEachFeature: onEachFeature
		}).addTo(map);
	}

	return {
		map: map
	}
})();

var Spinner = (function () {
	function _build() {
		var spinnerHTML = `<div class="spinner">
		<div class="lds-grid">
				<div></div>
				<div></div>
				<div></div>
				<div></div>
				<div></div>
				<div></div>
				<div></div>
				<div></div>
				<div></div>
			</div>
		</div>`;

		$('body').append(spinnerHTML)
	}

	function show() {
		_build();
		var target = document.querySelector('.spinner');
		target.classList.add("active");
	}

	function hide() {
		var target = document.querySelector('.spinner');
		target.classList.remove("active");
	}

	return {
		show: show,
		hide: hide
	}
})();

var NavTogller = (function () {
	return {
		init: init
	}

	function init() {
		var toggler = document.querySelector('.main-nav-toggle'),
			sidebar = document.querySelector('.page-sidebar'),
			nav = document.querySelector('.side-menu'),
			navItems = nav.querySelectorAll('li'),
			dropdown = document.querySelector('.nav-2-level');

		navItems.forEach(function (item) {
			item.addEventListener("click", function (e) {
				e.preventDefault();
				dropdown.classList.toggle('collapse')
			})
		})

		toggler.addEventListener("click", function (e) {
			sidebar.classList.toggle('opened');
		})
	}

})();

var TableVisits = {
	init: function () {
		var statesData = StatesJSON.get()
		var tableWrapper = document.querySelector('.table-states')
		var tbody = tableWrapper.querySelector('tbody')
		var tableRowsArray = []

		if (statesData.sum("visits") === 0 && statesData.sum("visitRate") === 0) {
			var rowEmpty = '<tr><td class="text-center" style="border-top:1px solid #eee;padding-top:2rem;display:table-cell" colspan="5">There is no data available</td></tr>';
			tbody.innerHTML = rowEmpty;
			return false;
		}

		function setFlag(id) {
			var flag = '';
			if (Number(id) > 0 && Number(id) <= 52)
				flag = 'us'
			else if (Number(id) > 52 && Number(id) <= 65)
				flag = 'ca'
			else if (Number(id) > 65 && Number(id) <= 76)
				flag = 'gb'
			else if (Number(id) > 76 && Number(id) <= 84)
				flag = 'au'
			return flag;
		}

		statesData.forEach(function (state) {
			var flag = setFlag(state.id)
			var row = `<tr>
						<th scope="row">${state.id}</th>
						<td class="text-left">
							<span class="flag-icon flag-icon-${flag}"></span>
						</td>
						<td class="text-left">${state.name}</td>
						<td class="text-center">${state.visits}</td>
						<td class="text-center">${state.visitRate}</td>
					</tr>`;

			tableRowsArray.push(row)
		})

		tbody.innerHTML = tableRowsArray.join("")

		$('#tableStates').DataTable();
	}
}


// create simalar method - data : OS, Browser, Ethnicity , Age
var StatesJSON = {
	get: function () {
		var States = [];

		for (let i = 0; i < statesData.features.length; i++) {
			var id = statesData.features[i].id
			var name = statesData.features[i].properties.name
			var visits = statesData.features[i].properties.visits
			var visitRate = statesData.features[i].properties.visitRate

			var state = {
				id: id,
				name: name,
				visits: visits,
				visitRate: visitRate
			}

			States.push(state);
		}

		return States;
	},

	update: function (states) {
		for (let i = 0; i < states.length; i++) {
			statesData.features[i].properties.visitRate = states[i].getAttribute('scale')
			statesData.features[i].properties.visits = states[i].getAttribute('visits')
		}
	}
}

var Request = {
	parseResponse: function (data) {
		/* XML Data */
		if (data === undefined) return false;

		var doc = data.responseXML;
		var successAttribute = doc.firstChild.getAttribute("success");
		if (successAttribute === "true") {
			// Data is parsed from XML to data structure
			var responseTag = doc.getElementsByTagName("response")[0];
			var criteria = responseTag.getAttribute("criteria"),
				visitsAll = responseTag.getAttribute("visits"),
				used = responseTag.getAttribute("used"),
				result = doc.getElementsByTagName("result"),
				states = doc.getElementsByTagName("state");

			// loops throught JSON data with states and updates the fields ( VISIT / VISIT RATE )
			StatesJSON.update(states)

			// Creates object with data to be send
			let newData = {
				statesData: statesData,
				visitsAll: visitsAll,
				used: used
			}

			// Send data to the methods updating the dashboard
			Utils.updateDashboard(newData);
		}

		setTimeout(() => {
			Spinner.hide();
		}, 1500)
	},
	httpRequest: function (url, callback) {
		var req = new XMLHttpRequest();
		var self = this;

		req.onreadystatechange = function () {
			if (req.readyState == 4 && req.status == 200) {
				self.parseResponse(this);
			}
		}
		req.open("GET", url)
		req.send()
	}
}

var Utils = {
	api: function () {
		return api = [
			"http://www.mocky.io/v2/59ad28562d00002b059b7d09",
			"http://www.mocky.io/v2/59a7e150100000710b8374b7",
			"http://www.mocky.io/v2/59ad47ce2d000012079b7d86",
			"http://www.mocky.io/v2/59ad4ac42d00003d079b7d91",
			"http://www.mocky.io/v2/59ad4b962d000040079b7d93",
			"http://www.mocky.io/v2/59b9235f3a00003801f7f906"
		];
	},

	updateDashboard: function (data) {
		LeafletMap.map.update(data.statesData, data.visitsAll, data.used);
		TableVisits.init();
	},

	randomElement: function (array) {
		return Math.floor(Math.random() * array.length)
	}
}


$(document).ready(function () {
	NavTogller.init();

	// Table - Visits, Percentage
	TableVisits.init();

	// XML data
	var ChartController = {
		// * Visits, Age, Ethnicity		
		EasyPieInit: function () {
			var easyPieData = [{
					class: ".chart-1",
					color: '#5a68bf',
				},
				{
					class: ".chart-2",
					color: '#18c5a9'
				},
				{
					class: ".chart-3",
					color: '#3499e2'
				}
			];
			var easypie = EasyPie.init(easyPieData);
		},

		// * Visitor browser
		BarChartInit: function () {
			var ctxBar = document.getElementById("browser_chart").getContext("2d");
			var lineData = [100, 180, 360, 300, 127, 80];
			var labelsArray = ["88%", "8%", "3.34%", "2%", "1.8%", "0.33%"];
			var barChart = BarChart.init(ctxBar, lineData, labelsArray);
		},
		init: function () {
			this.EasyPieInit();
			this.BarChartInit();
		}
	}

	ChartController.init();

	var ctx = document.getElementById("deviceChart").getContext('2d');
	var donutData = {
		datasets: [{
			data: [33, 88, 5],
			backgroundColor: ["#5c6bc0", "#18c5a9", "#ff4081"],
			borderWidth: 2
		}]
	}
	var donut = Doughnut.init(ctx, donutData);

	$('.btn-report').on('click', function () {
		// call ajax here
		Spinner.show();
		var api = Utils.api()
		var rand = Utils.randomElement(api)
		Request.httpRequest(api[rand], Utils.httpRequest);
		Doughnut.update(donut, [5]);
		EasyPie.update(".chart-1", 5);
		EasyPie.update(".chart-2", 15);
		EasyPie.update(".chart-3", 80);
	})

})