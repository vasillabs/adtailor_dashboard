const express = require('express'),
      bodyParser = require('body-parser'),
      path = require('path');

    var app = express();

    
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());

    app.use(express.static(path.join(__dirname, 'public')));
    
    app.get('/', (req, res) => {
        res.send('called');
    })
    
    const PORT = 5000;

    var server = app.listen(PORT , function () {
        console.log(`The app is running on port ${PORT}`);        
    });


